package com.example.homework2.models;

public class Photo extends CellList{

    private long albumId;
    private long id;
    private String title;
    private String url;

    public Photo( long albumId, long id, String title, String url) {
        super(CellListTyper.PHOTO);
        this.albumId = albumId;
        this.id = id;
        this.title = title;
        this.url = url;
    }

    public long getAlbumId() {
        return albumId;
    }

    public void setAlbumId(long albumId) {
        this.albumId = albumId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
