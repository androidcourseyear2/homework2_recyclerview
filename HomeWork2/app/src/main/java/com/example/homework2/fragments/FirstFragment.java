package com.example.homework2.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.homework2.R;
import com.example.homework2.adapters.MyAdapterUser;
import com.example.homework2.interfaces.FragmentCommunication;
import com.example.homework2.interfaces.OnItemClick;
import com.example.homework2.models.Album;
import com.example.homework2.models.Post;
import com.example.homework2.models.User;
import com.example.homework2.singletons.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.homework2.constants.Constants.ADDRESS;
import static com.example.homework2.constants.Constants.BASE_URL;
import static com.example.homework2.constants.Constants.BODY;
import static com.example.homework2.constants.Constants.BS;
import static com.example.homework2.constants.Constants.CATCHPHRASE;
import static com.example.homework2.constants.Constants.CITY;
import static com.example.homework2.constants.Constants.COMPANY;
import static com.example.homework2.constants.Constants.EMAIL;
import static com.example.homework2.constants.Constants.GEO;
import static com.example.homework2.constants.Constants.ID;
import static com.example.homework2.constants.Constants.LAT;
import static com.example.homework2.constants.Constants.LNG;
import static com.example.homework2.constants.Constants.NAME;
import static com.example.homework2.constants.Constants.PHONE;
import static com.example.homework2.constants.Constants.STREET;
import static com.example.homework2.constants.Constants.SUITE;
import static com.example.homework2.constants.Constants.TITLE;
import static com.example.homework2.constants.Constants.USERID;
import static com.example.homework2.constants.Constants.USERNAME;
import static com.example.homework2.constants.Constants.WEBSITE;
import static com.example.homework2.constants.Constants.ZIPCODE;

public class FirstFragment extends Fragment {
    private FragmentCommunication fragmentCommunication;

    ArrayList<User> users = new ArrayList<User>();
    //  MyAdapterUser adapterUser=null;

    MyAdapterUser adapterUser = new MyAdapterUser(users, new OnItemClick() {
        @Override
        public void userItemClick(User user) {
            if (fragmentCommunication != null) {
                fragmentCommunication.addSecondFragment(user);
                Toast.makeText(getContext(), "Go fragment 2", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void userItemButtonClick(User user) {

            Toast.makeText(getContext(), "Posts for User ", Toast.LENGTH_SHORT).show();
            getPosts(user);
        }

        @Override
        public void albumItemClick(Album album) {

        }
    });

    public static FirstFragment newInstance() {

        Bundle args = new Bundle();

        FirstFragment fragment = new FirstFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getUsers();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        RecyclerView userList = (RecyclerView) view.findViewById(R.id.user_list);
        userList.setLayoutManager(linearLayoutManager);
        userList.setAdapter(adapterUser);

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FragmentCommunication) {
            fragmentCommunication = (FragmentCommunication) context;
        }
    }

    public void getUsers() {

        MySingleton volleySingleton=MySingleton.getInstance(getContext());

        RequestQueue queue=volleySingleton.getRequestQueue();

        String url = BASE_URL + "/users";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        handleUserResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        queue.add(stringRequest);
    }

    public void handleUserResponse(String response) {

        try {
            JSONArray userJsonArray = new JSONArray(response);
            for (int index = 0; index < userJsonArray.length(); index++) {
                JSONObject userJsonObject = (JSONObject) userJsonArray.get(index);
                if (userJsonArray != null) {

                    long id = userJsonObject.getLong(ID);
                    String name = userJsonObject.getString(NAME);
                    String username = userJsonObject.getString(USERNAME);
                    String email = userJsonObject.getString(EMAIL);
                    String phone = userJsonObject.getString(PHONE);
                    String website = userJsonObject.getString(WEBSITE);

                    JSONObject adrressJsonObject = userJsonObject.getJSONObject(ADDRESS);
                    String street = adrressJsonObject.getString(STREET);
                    String suite = adrressJsonObject.getString(SUITE);
                    String city = adrressJsonObject.getString(CITY);
                    String zipcode = adrressJsonObject.getString(ZIPCODE);

                    JSONObject geoJsonObject = adrressJsonObject.getJSONObject(GEO);
                    String lat = geoJsonObject.getString(LAT);
                    String lng = geoJsonObject.getString(LNG);

                    JSONObject companyJsonObject = userJsonObject.getJSONObject(COMPANY);
                    String nameCo = companyJsonObject.getString(NAME);
                    String catchPhrase = companyJsonObject.getString(CATCHPHRASE);
                    String bs = companyJsonObject.getString(BS);

                    User user = new User(id, name, username, email);
                    users.add(user);

                }


            }
            adapterUser.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getPosts(User user) {

        MySingleton volleySingleton=MySingleton.getInstance(getContext());
        RequestQueue queue=volleySingleton.getRequestQueue();
        String url=BASE_URL+"/users/";
        url=url+user.getId()+"/posts";

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            handlePostsResponse(response,user);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "Error!", Toast.LENGTH_SHORT).show();
                    }
                });
        queue.add(stringRequest);
    }

    public void handlePostsResponse(String response,User user) throws JSONException {
        user.getPosts().clear();
        JSONArray postsJsonArray=new JSONArray(response);

        for(int index=0;index< postsJsonArray.length();index++)
        {
            JSONObject postJsonObject=postsJsonArray.getJSONObject(index);
            if(postJsonObject!=null)
            {
                long userId = postJsonObject.getLong(USERID);
                long id = postJsonObject.getLong(ID);
                String title = postJsonObject.getString(TITLE);
                String body = postJsonObject.getString(BODY);

                Post post=new Post(userId,id,title,body);

                if(!user.getPosts().contains(post))
                {
                    user.getPosts().add(post);
                }
            }
        }
        adapterUser.notifyDataSetChanged();
    }
}
