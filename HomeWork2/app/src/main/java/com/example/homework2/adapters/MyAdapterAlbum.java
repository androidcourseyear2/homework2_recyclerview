package com.example.homework2.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.homework2.R;
import com.example.homework2.interfaces.OnItemClick;
import com.example.homework2.models.Album;
import com.example.homework2.models.User;

import java.util.ArrayList;

public class MyAdapterAlbum extends RecyclerView.Adapter<MyAdapterAlbum.AlbumViewHolder> {

    private ArrayList<Album> albums;
    private OnItemClick onItemClick;

    public MyAdapterAlbum(ArrayList<Album> albums, OnItemClick onItemClick) {
        this.albums=albums;
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public AlbumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater= LayoutInflater.from(parent.getContext());


        View view =inflater.inflate(R.layout.album_item,parent,false);
       AlbumViewHolder albumViewHolder=new AlbumViewHolder(view);

        return albumViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumViewHolder holder, int position) {

        Album album=(Album) albums.get(position);
        ((MyAdapterAlbum.AlbumViewHolder)holder).bind(album);


    }

    @Override
    public int getItemCount() {

            return this.albums.size();

    }

    class AlbumViewHolder extends RecyclerView.ViewHolder{

        private TextView album;
         View view;

        public AlbumViewHolder(View view)
        {
            super(view);
            this.album=view.findViewById(R.id.album);
            this.view=view;
        }

        void bind(Album albumObj)
        {
            album.setText(albumObj.getTitle());

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onItemClick!=null)
                    {
                        onItemClick.albumItemClick(albumObj);
                        notifyItemChanged(getAdapterPosition());
                    }
                }
            });

        }

    }
}
