package com.example.homework2.adapters;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.homework2.interfaces.OnItemClick;
import com.example.homework2.models.Album;
import com.example.homework2.models.Photo;
import com.example.homework2.models.User;

import java.util.ArrayList;

public class MyAdapterPhoto extends RecyclerView.Adapter<MyAdapterPhoto.PhotoViewHolder>{

    private ArrayList<Photo> photos;
   //private OnItemClick onItemClick;
    public MyAdapterPhoto(ArrayList<Photo> photos) {
        this.photos=photos;

    }

    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
        //to do
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoViewHolder holder, int position) {

        //to do
    }

    @Override
    public int getItemCount() {
        return 0;
        //to do
    }

    class PhotoViewHolder extends RecyclerView.ViewHolder{

        private View view;


        public PhotoViewHolder(@NonNull View view) {
            super(view);
        }
    }
}
